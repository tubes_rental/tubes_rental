package com.example.dell.tubes_rent;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.UUID;

import static android.text.TextUtils.isEmpty;

public class create extends AppCompatActivity {


    private Button btSubmit;
    private EditText merk;
    private EditText plat;
    private EditText Harga;
    Button chooseImg;
    ImageView imgView;
    int PICK_IMAGE_REQUEST = 111;
    Uri filePath;
    private FirebaseDatabase mFirebaseInstance;
    private String userId;
    private String imagepath;
    private DatabaseReference database;

    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://tubes-5d9d4.appspot.com");    //change the url according to your firebase app

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        mFirebaseInstance = FirebaseDatabase.getInstance();


        database = mFirebaseInstance.getReference("motors");

        merk = (EditText) findViewById(R.id.menu);
        plat = (EditText) findViewById(R.id.desc);
        Harga = (EditText) findViewById(R.id.price);
        btSubmit = (Button) findViewById(R.id.btnadd);
        chooseImg = (Button)findViewById(R.id.button);
        imgView = (ImageView)findViewById(R.id.imageButton);
        userId = database.push().getKey();



        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isEmpty(merk.getText().toString()) && !isEmpty(plat.getText().toString()) && !isEmpty(Harga.getText().toString())) {


                    submitMenu(new motor(
                            merk.getText().toString(),
                            Harga.getText().toString(),
                            plat.getText().toString(),
                            imagepath,
                            "Avaible",
                            userId)

                    );

                }else {

                    Toast.makeText(create.this, "Data Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                    InputMethodManager imm = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(
                            merk.getWindowToken(), 0);
                }



            }
        });


        //milih gambar
        chooseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //nampilin gambar yg dipilih
                imgView.setImageBitmap(Bitmap.createScaledBitmap(bitmap,280,240,false));
//                Bitmap b = BitmapFactory.decodeByteArray(bitmapProfilePic , 0, bitmapProfilePic .length)
//                ivProfilePic.setImageBitmap(Bitmap.createScaledBitmap(b, 120, 120, false));
                uploadImage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void uploadImage() {



        if(filePath != null)
        {

            final String a = UUID.randomUUID().toString();
            StorageReference ref = storageRef.child("images/"+a);


            //upload image ke firebase storage
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            imagepath = a;
                            Toast.makeText(create.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            Toast.makeText(create.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());

                        }
                    });
        }
    }

    private void submitMenu(motor motorlis) {


        if (TextUtils.isEmpty(userId)) {
            userId = database.push().getKey();
        }

        motor menu1 = motorlis;//datanya

        database.child(userId).setValue(menu1); //upload firebase
        Toast.makeText(create.this, "Data Berhasil ditambahkan", Toast.LENGTH_SHORT).show();

        addUserChangeListener();


    }

    private void addUserChangeListener() {
        // User data change listener
        database.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                motor menu = dataSnapshot.getValue(motor.class);


                Intent intent = new Intent(create.this, home.class);
                startActivity(intent);

                // Check for null
                if (menu == null) {

                    return;
                }



            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

}
