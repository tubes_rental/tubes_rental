package com.example.dell.tubes_rent;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by DELL on 24/03/2019.
 */

public class RecyleviewAdapter extends RecyclerView.Adapter<RecyleviewAdapter.ViewHolder>  {
    private final ArrayList<motor> Datalist;
    private final LayoutInflater mInflater;

    class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        public  TextView merk, harga,status;
        public ImageView img;
        final RecyleviewAdapter mAdapter;
        public ViewHolder(View itemView, RecyleviewAdapter adapter) {
            super(itemView);
            merk = itemView.findViewById(R.id.merk);
            harga = itemView.findViewById(R.id.harga);
            status = itemView.findViewById(R.id.status);
            img = itemView.findViewById(R.id.imgm);
            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int mPosition = getLayoutPosition();

//            String element = mWordList.get(mPosition);
//            mWordList.set(mPosition, "Clicked! " + element);

           Intent intent = new Intent(itemView.getContext(),DetailActivity.class );

//            Intent intent = new Intent(itemView.getContext(),konfirmasi.class );


            intent.putExtra("harga", Datalist.get(mPosition).harga);
            intent.putExtra("plat", Datalist.get(mPosition).plat);
            intent.putExtra("merk", Datalist.get(mPosition).merk);
            intent.putExtra("gambar", Datalist.get(mPosition).image);
            intent.putExtra("id", Datalist.get(mPosition).uid);

            itemView.getContext().startActivity(intent);

            mAdapter.notifyDataSetChanged();
        }
    }

    public RecyleviewAdapter(Context context, ArrayList<motor> motorlist) {
        mInflater = LayoutInflater.from(context);
        this.Datalist = motorlist;
    }

    @Override
    public RecyleviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(
                R.layout.motor_item, parent, false);


        return new ViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(final RecyleviewAdapter.ViewHolder holder, int position) {
        motor mCurrent = Datalist.get(position);

        final StorageReference islandRef = FirebaseStorage.getInstance().getReference().child("images/" + Datalist.get(position).image);

        final long ONE_MEGABYTE =  1024 * 1024;
        islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
               // ImageView image = (ImageView) findViewById(R.id.imageView1);

                holder.img.setImageBitmap(Bitmap.createScaledBitmap(bmp, holder.img.getWidth(),
                        holder.img.getHeight(), false));


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                holder.img.setImageResource(R.drawable.vario);
            }
        });
        holder.merk.setText(mCurrent.merk);
        holder.harga.setText(mCurrent.harga);
        holder.status.setText(mCurrent.status);
    }

    @Override
    public int getItemCount() {
        return Datalist.size();
    }


}
