package com.example.dell.tubes_rent;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        final Intent inte = getIntent();

        Button add = (Button) findViewById(R.id.add);
        final ImageView imageView = (ImageView)findViewById(R.id.imageView);
        TextView platnom = (TextView) findViewById(R.id.plat);
        TextView harga = (TextView) findViewById(R.id.Harga);
        TextView nama = (TextView) findViewById(R.id.merk);


        platnom.setText( inte.getStringExtra("plat"));
        harga.setText(inte.getStringExtra("harga"));
        nama.setText(inte.getStringExtra("merk"));

        final StorageReference islandRef = FirebaseStorage.getInstance().getReference().child("images/" + inte.getStringExtra("gambar"));

        final long ONE_MEGABYTE =  1024 * 1024;
        islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                // ImageView image = (ImageView) findViewById(R.id.imageView1);

                imageView.setImageBitmap(Bitmap.createScaledBitmap(bmp, imageView.getWidth(),
                        imageView.getHeight(), false));


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                imageView.setImageResource(R.drawable.vario);
            }
        });



        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, BookingActivity.class);
                intent.putExtra("harga", inte.getStringExtra("harga"));
                intent.putExtra("plat", inte.getStringExtra("plat"));
                intent.putExtra("gambar",inte.getStringExtra("gambar"));
                intent.putExtra("id", inte.getStringExtra("id"));

                startActivity(intent);
            }
        });


    }
}
