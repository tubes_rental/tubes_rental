package com.example.dell.tubes_rent;

        import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by DELL on 25/03/2019.
 */

@IgnoreExtraProperties
public class UserModel {

    public String name;
    public String email;
    public String nik;
    public String telp;
    public String alamat;
    public String password;


    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public UserModel() {

    }

    public UserModel(String name, String email, String nik, String telp, String alamat, String password) {
        this.name = name;
        this.email = email;
        this.nik = nik;
        this.telp = telp;
        this.alamat = alamat;
        this.password = password;
    }


}
