package com.example.dell.tubes_rent;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DELL on 18/04/2019.
 */
@IgnoreExtraProperties
public class motor {

    public String merk;
    public String harga;
    public String plat;
    public String image;
    public String status;
    public String uid;
    public Map<String, Boolean> stars = new HashMap<>();




    public motor() {

    }

    public motor(String merk, String harga, String plat, String image, String status, String uid) {
        this.merk = merk;
        this.harga = harga;
        this.plat = plat;
        this.image = image;
        this.status = status;
        this.uid = uid;

    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("merk", merk);
        result.put("harga", harga);
        result.put("plat", plat);
        result.put("image", image);
        result.put("status", status);
        result.put("uid", uid);

        return result;
    }

}
