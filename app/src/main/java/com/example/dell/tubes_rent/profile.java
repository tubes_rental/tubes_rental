package com.example.dell.tubes_rent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class profile extends AppCompatActivity {

    private DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        database = FirebaseDatabase.getInstance().getReference();

        final TextView nama = (TextView) findViewById(R.id.Pnama);
        final TextView nik = (TextView) findViewById(R.id.Pnik);
        final TextView alamat = (TextView) findViewById(R.id.Palamat);
        final TextView email = (TextView) findViewById(R.id.pemail);

        database.child("users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                UserModel userModel = dataSnapshot.getValue(UserModel.class);
                nama.setText(userModel.name);
                nik.setText(userModel.nik);
                alamat.setText(userModel.alamat);
                email.setText(userModel.email);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }
}
