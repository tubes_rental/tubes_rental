package com.example.dell.tubes_rent;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.text.TextUtils.isEmpty;

public class BookingActivity extends AppCompatActivity {

  //  Integer totalharga = 0;
    String time = "" ;
    String date = "";
    String tujuan = "";
    private int mYear, mMonth, mDay, mHour, mMinute;
    String tglpulang  = "";
    String waktupulang = "";
    Integer hargaall = 0;
    private FirebaseDatabase mFirebaseInstance;
    private String userId;
    String platnomer = "";
    String Imageref = "";
    String iduser = "";
    String name ="";
    String telp= "";

    private DatabaseReference database, database2, database3;

    FirebaseStorage storage = FirebaseStorage.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        mFirebaseInstance = FirebaseDatabase.getInstance();

        database = mFirebaseInstance.getReference("transaksi");
        database2 = mFirebaseInstance.getReference("motors");
        database3 = FirebaseDatabase.getInstance().getReference();

        final TextView pilhtglawal = (TextView) findViewById(R.id.tgl1);
        final TextView pilhjamawal = (TextView) findViewById(R.id.jam1);
        final TextView pilhtglakhir = (TextView) findViewById(R.id.tgl2);
        final TextView pilhjamakhir = (TextView) findViewById(R.id.jam22);
        final TextView totalharga = (TextView) findViewById(R.id.totalharga);
        final EditText totalhari = (EditText) findViewById(R.id.hari);

        Intent intent = getIntent();
        totalharga.setText(intent.getStringExtra("harga"));
        hargaall = Integer.parseInt(intent.getStringExtra("harga"));
        platnomer = intent.getStringExtra("plat");
        Imageref = intent.getStringExtra("gambar");
        userId = database.push().getKey();
        iduser = intent.getStringExtra("id");

        Button btonok = (Button) findViewById(R.id.btnok);

        pilhtglawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(BookingActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year ;
                                pilhtglawal.setText(date);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        pilhjamawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(BookingActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                time  = hourOfDay + ":" + minute;
                                pilhjamawal.setText(time);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

            }
        });

        pilhtglakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(BookingActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year ;

                                pilhtglakhir.setText(date);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        pilhjamakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(BookingActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                time  = hourOfDay + ":" + minute;
                                pilhjamakhir.setText(time);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

            }
        });

        totalhari.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(totalhari.getText())) {
                    int a = Integer.parseInt(totalhari.getText().toString());
                    int b = hargaall;
                    int c = a * b;
                    totalharga.setText(String.valueOf(c));
                }

            }
        });

        database3.child("users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                UserModel userModel = dataSnapshot.getValue(UserModel.class);

                name = userModel.name;
                telp = userModel.telp;
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        btonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isEmpty(pilhtglawal.getText().toString()) &&
                        !isEmpty(pilhtglakhir.getText().toString()) &&
                        !isEmpty(pilhjamawal.getText().toString())&&
                        !isEmpty(pilhjamakhir.getText().toString())&&
                        !isEmpty(totalhari.getText().toString())&&
                        !isEmpty(totalharga.getText().toString())

                        ) {


                    submitMenu(new transaksi(
                            pilhtglawal.getText().toString(),
                            pilhtglakhir.getText().toString(),
                            pilhjamawal.getText().toString(),
                            pilhjamakhir.getText().toString(),
                            totalhari.getText().toString(),
                            totalharga.getText().toString(),
                            name,telp
                    ));

                }else {

                    Toast.makeText(BookingActivity.this, "Data Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();

//                    InputMethodManager imm = (InputMethodManager)
//                            getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(
//                            merk.getWindowToken(), 0);
                }

            }
        });
    }

    private void submitMenu(transaksi motorlis) {

        if (TextUtils.isEmpty(userId)) {
            userId = database.push().getKey();
        }

        transaksi menu1 = motorlis;//datanya

        database.child(userId).setValue(menu1); //upload firebase
  //      Toast.makeText(BookingActivity.this, "Data Berhasil ditambahkan", Toast.LENGTH_SHORT).show();

        addUserChangeListener();

    }

    private void addUserChangeListener() {
        // User data change listener
        database.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
  //              motor menu = dataSnapshot.getValue(motor.class);


             //   database2.child(userId).child("status").setValue("booked");
//                Query tes = database2.child(userId).child("plat").equalTo(platnomer);
 //               String key1 = database2.child(userId).push().getKey();
 //               motor post = new motor(userId,String.valueOf(hargaall), platnomer,Imageref,"Booked");
//                Map<String, Object> postValues = post.toMap();
//                Map<String, Object> childUpdates = new HashMap<>();
//                childUpdates.put(userId + key1, postValues);
//                database2.updateChildren(childUpdates);

                database2.child(iduser).child("status").setValue("Booked");

                Toast.makeText(BookingActivity.this, "Pemesanan Berhasil", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(BookingActivity.this, home.class);
                startActivity(intent);



                // Check for null
//                if (menu == null) {
//                    Log.wtf("","");
//
//                    return;
//                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.wtf("","");

            }
        });
    }

    private void updateUser(String key, String id, String name) {


//        User user = new User(id, name);
//        Map<String, Object> postValues = post.toMap();
//        Map<String, Object> childUpdates = new HashMap<>();
//        childUpdates.put("/data/" + key, postValues);
//        db.updateChildren(childUpdates);

//        String key1 = database2.child("merk").push().getKey();
//        motor post = new motor(userId,String.valueOf(hargaall), platnomer,Imageref,"Booked");
//        Map<String, Object> postValues = post.toMap();
//
//        Map<String, Object> childUpdates = new HashMap<>();
//        childUpdates.put("/motors/" + key, postValues);
//        childUpdates.put("/user-motors/" + userId + "/" + key, postValues);
//
//        database2.updateChildren(childUpdates);
//
//        Toast.makeText(BookingActivity.this, "Pemesanan Berhasil", Toast.LENGTH_LONG).show();
//        Intent intent = new Intent(BookingActivity.this, home.class);
//        startActivity(intent);

    }

    private void updateBarang() {
        /**
         * Baris kode yang digunakan untuk mengupdate data barang
         * yang sudah dimasukkan di Firebase Realtime Database
         */

        database.child("merk") //akses parent index, ibaratnya seperti nama tabel
                .child(platnomer) //select barang berdasarkan key
                .setValue("") //set value barang yang baru
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        /**
                         * Baris kode yang akan dipanggil apabila proses update barang sukses
                         */

                    }
                });
    }

}
