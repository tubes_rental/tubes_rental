package com.example.dell.tubes_rent;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class historyadapter extends RecyclerView.Adapter<historyadapter.ViewHolder>  {

    private final ArrayList<transaksi> Datalist;
    private final LayoutInflater mInflater;

    class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        public TextView jam1, jam2,tgl1,tgl2,hari,harga;

        final historyadapter mAdapter;
        public ViewHolder(View itemView, historyadapter adapter) {
            super(itemView);
//            merk = itemView.findViewById(R.id.merk);
//            harga = itemView.findViewById(R.id.harga);
//            status = itemView.findViewById(R.id.status);
//            img = itemView.findViewById(R.id.imgm);
            jam1 = itemView.findViewById(R.id.textView14);
            jam2 = itemView.findViewById(R.id.textView15);
            tgl1 = itemView.findViewById(R.id.textView11);
            tgl2 = itemView.findViewById(R.id.textView12);
            hari = itemView.findViewById(R.id.textView17);
            harga = itemView.findViewById(R.id.textView19);



            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int mPosition = getLayoutPosition();

//            String element = mWordList.get(mPosition);
//            mWordList.set(mPosition, "Clicked! " + element);

//            Intent intent = new Intent(itemView.getContext(),DetailActivity.class );
//            intent.putExtra("harga", Datalist.get(mPosition).harga);
//            intent.putExtra("plat", Datalist.get(mPosition).plat);
//            intent.putExtra("merk", Datalist.get(mPosition).merk);
//            intent.putExtra("gambar", Datalist.get(mPosition).image);
//            intent.putExtra("id", Datalist.get(mPosition).uid);
//
//            itemView.getContext().startActivity(intent);

            mAdapter.notifyDataSetChanged();
        }
    }

    public historyadapter(Context context, ArrayList<transaksi> motorlist) {
        mInflater = LayoutInflater.from(context);
        this.Datalist = motorlist;
    }

    @Override
    public historyadapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(
                R.layout.history_item, parent, false);


        return new historyadapter.ViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(final historyadapter.ViewHolder holder, int position) {
        transaksi mCurrent = Datalist.get(position);

        holder.jam1.setText(mCurrent.jamawal);
        holder.jam2.setText(mCurrent.jamakhir);
        holder.tgl1.setText(mCurrent.tglawal);
        holder.tgl2.setText(mCurrent.tglakhir);
        holder.hari.setText(mCurrent.hari);
        holder.harga.setText(mCurrent.bayar);


//        holder.merk.setText(mCurrent.merk);
//        holder.harga.setText(mCurrent.harga);
//        holder.status.setText(mCurrent.status);
    }

    @Override
    public int getItemCount() {
        return Datalist.size();
    }
}
